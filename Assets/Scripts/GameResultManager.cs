﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameResultManager : MonoBehaviour {
    public GameObject medal;
    public Sprite[] medals;

    GameObject theCamera;

    // Use this for initialization
    void Start () {
        theCamera = GameObject.Find ("MainCamera");
    }

    void setMedal(int score) {
        GameObject medal = GameObject.Find("GameResult/ResultBoard/Medal");
        if (score < 10){
            medal.GetComponent<Image>().enabled = false;
            return;
        }
        if (score > 39){
            medal.GetComponent<Image>().sprite = medals[3];
        } else if (score > 29) { 
            medal.GetComponent<Image>().sprite = medals[2];
        } else if (score > 19) {
            medal.GetComponent<Image>().sprite = medals[1];
        } else if (score > 9) {
            medal.GetComponent<Image>().sprite = medals[0];
        }
        GameObject.Find("GameResult/ResultBoard/Medal/SparkleEffect").GetComponent<SparkleEffect>().turnOnTheSparkles();
    }

    public void endGame(int score) {
        updateScores(score);
    }

    public void updateScores(int score) {
        if (score > PlayerPrefs.GetInt ("highScore")) {
            PlayerPrefs.SetInt("highScore", score);
        }
        setMedal(score);
        GameObject.Find("GameResult/ResultBoard/CurrentScore").GetComponent<Text>().text = score.ToString();
        GameObject.Find("GameResult/ResultBoard/HighScore").GetComponent<Text>().text = (PlayerPrefs.GetInt("highScore")).ToString();
    }

    public void restartLevel() {
        theCamera.GetComponent<FadeManager>().LoadLevel("Game", 1f);
    }


}

﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {
    public float ResetPosition;
    public float OffScreen;
    bool isMoving = false;

	// Use this for initialization
	void Start () {
        isMoving = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (isMoving) {
            transform.Translate (-GameManager.speed, 0.0f, 0.0f);
//            Debug.Log ("ground" + GameManager.speed);
            if (this.transform.position.x < OffScreen) {
                this.transform.position += new Vector3 ((ResetPosition), 0, 0);
            }
        }
	}

    public void stopGround() {
        isMoving = false;
    }
 
}

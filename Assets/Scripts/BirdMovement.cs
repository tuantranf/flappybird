﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class BirdMovement : MonoBehaviour
{
    public int points = 0;
    public AudioClip s_die;
    public AudioClip s_hit;
    public AudioClip s_swing;
    public AudioClip s_point;
    
    Animator animator;
    GameManager manager;

    GameObject thePipes;
    GameObject gameScore;
    GameObject theCamera;

    void Start () {
        animator = transform.GetComponentInChildren<Animator>();
        if (GameManager.GameStatus == 0) {
            animator.SetInteger ("ChoosePlayer", Random.Range (1, 3));
        }
        manager = FindObjectOfType<GameManager>();

        thePipes = GameObject.Find ("Pipes");
        gameScore= GameObject.Find ("GameScore");
        theCamera = GameObject.Find ("MainCamera");
    }

    // Update is called once per frame
    void Update () {
//        Debug.Log ("GameStatus = " + GameManager.GameStatus);

        if (GameManager.GameStatus == 2) {
            // game over
            return;
        }

        // game playing
        if (GameManager.GameStatus == 1) {
            if (Input.GetMouseButtonDown (0)) {
                Jump ();
            }

            if (rigidbody2D.velocity.y < -1f && animator.speed > 0) {
                animator.speed = 0;
            }

			if (rigidbody2D.velocity.y > -3f) {
		
                // Flying up
                transform.rotation = Quaternion.AngleAxis (GameManager.jumAngle, Vector3.forward);
            } else {
                // Flying down
				transform.rotation = Quaternion.AngleAxis (GameManager.jumAngle + 15 * rigidbody2D.velocity.y, Vector3.forward);
            }
				
            return;
        }

        // game ready
        if (GameManager.GameStatus == 0) {
            if (Input.GetMouseButtonDown (0)) {
                manager.gameStart ();
            }
            
            // bird bounce
			transform.Translate (0.0f, 0.005f * Mathf.Sin (Time.time * 5), 0.0f);

            return;
        }

        // game title
        if (GameManager.GameStatus == -1) {
            // bird bounce
			transform.Translate (0.0f, 0.005f * Mathf.Sin (Time.time * 5), 0.0f);

            return;
        }

        Debug.LogError ("WTF???");
    }

    void OnTriggerEnter2D(Collider2D c){

        if (GameManager.GameStatus == 2) {
            return;
        }

        Debug.Log ("OnTriggerEnter2D: " + c.gameObject.tag);

        if (c.gameObject.tag == "ScoreArea") {
            // score sound
            AudioSource.PlayClipAtPoint(s_point, transform.position);

            points++;
            gameScore.GetComponent<ScoreManager>().updateScore(points);
			thePipes.GetComponent<PipeManager> ().createPipes (c.gameObject.transform.parent.transform.position.x + GameManager.pipeInterval);
            Destroy(c.collider2D);
        }

        if (c.gameObject.tag == "Pipe") {
            Debug.Log("OnTriggerEnter2D:" + c.gameObject.tag);
            // hit sound
            AudioSource.PlayClipAtPoint(s_hit, transform.position);
            
            theCamera.GetComponent<FadeManager>().Glance();
            StopFly ();
                
            // die sound
            AudioSource.PlayClipAtPoint(s_die, transform.position);
                
            manager.gameOver(points);
        }
    }
    
    
    void OnCollisionEnter2D(Collision2D col){
        
        if (GameManager.GameStatus == 2) {
            return;
        }

        Debug.Log("OnCollisionEnter2D:" + col.gameObject.tag);

        if (col.gameObject.tag == "Ground") {

            // hit sound
            AudioSource.PlayClipAtPoint(s_hit, transform.position);

            theCamera.GetComponent<FadeManager>().Glance();
            StopFly ();

            manager.gameOver(points);
        }
    }

    
    void Jump() {
        Debug.Log("Jump ");

        // swing
        AudioSource.PlayClipAtPoint(s_swing, transform.position);;
        animator.speed = 2.5f;

        rigidbody2D.velocity = Vector2.up * GameManager.jump;
    }
  
    
    void StopFly() {
        Debug.Log("Stop ");


//        Debug.Log ("zzzzz: " + transform.eulerAngles.z);
        // dropping
        transform.rotation = Quaternion.AngleAxis (90, Vector3.back);

        animator.speed = 0;
    }
    
}
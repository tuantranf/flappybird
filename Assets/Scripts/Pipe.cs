﻿using UnityEngine;
using System.Collections;

public class Pipe : MonoBehaviour {
    bool isMoving = false;

    // Use this for initialization
    void Start () {
    }

    // Update is called once per frame
    void Update () {
        if (isMoving) {
            transform.Translate (-GameManager.speed, 0.0f, 0.0f);
//            Debug.Log ("pipe" + GameManager.speed);
            if (transform.position.x <= GameManager.deletePosX) {
                Destroy (gameObject);
            }
        }
    }

    public void startPipe() {
        isMoving = true;
    }

    public void stopPipe() {
        isMoving = false;
        foreach(Collider2D c in GetComponentsInChildren<Collider2D>()){
            Destroy(c);
        }
    }

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    // Use this for initialization
    void Start () {

    }

    public void updateScore(int newScore) {
        GetComponentInChildren<Text>().text = newScore.ToString();
    }
}

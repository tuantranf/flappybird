﻿using UnityEngine;
using System.Collections;

public class TitleManager : MonoBehaviour {

    GameObject theCamera;

    void Awake() {
        QualitySettings.vSyncCount = 0; // VSyncをOFFにする
        Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定
    }

    // Use this for initialization
    void Start () {
        theCamera = GameObject.Find ("MainCamera");
        GameManager.GameStatus = -1;
    }
    
    public void moveToNext() {
        theCamera.camera.GetComponent<FadeManager>().LoadLevel("Game", 1f);
    }
}
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SparkleEffect : MonoBehaviour {
    public Sprite[] sprites;
    public float fps;
    public float xRange;
    public float yRange;

    Vector3 prevMov;
    
    bool moved = false;
    bool isActive = false;
    
    // Use this for initialization
    void Start () {
        prevMov = Vector3.zero;
    }
    
    // Update is called once per frame
    void Update () {
        if(!isActive){
            return;
        }
        int index = (int)(Time.timeSinceLevelLoad * fps);
        index = index % sprites.Length;
        
        gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(sprites[index].rect.width, sprites[index].rect.height);
        gameObject.GetComponent<Image>().sprite = sprites[index];
        
        if (index == 0){
            if(!moved){
                transform.Translate(-prevMov); // Reset position to original
                prevMov = new Vector3(Random.Range(-1f, 1f) * xRange, Random.Range(-1f, 1f) * yRange, 0);
                transform.Translate(prevMov);
                
                moved = true;
            }
        } else { // Only move once
            moved = false;
        }
    }
    public void turnOnTheSparkles(){
        isActive = true;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Background : MonoBehaviour {
    // backgrounds
    public Sprite[] backs;


    // Use this for initialization
    void Start () {
        GameObject back = GameObject.Find ("Background/Back");
        back.GetComponent<SpriteRenderer> ().sprite = backs[Random.Range(0, backs.Length)];
    }

    public void stopGrounds() {
        GameObject ground = GameObject.Find ("Background/Ground");
        foreach(Ground child in ground.GetComponentsInChildren<Ground>()){
            child.stopGround();
        }
    }
}
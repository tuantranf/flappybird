﻿using UnityEngine;
using System.Collections;

public class PipeManager : MonoBehaviour {
    public GameObject pipePrefab;

    // Use this for initialization
    void Start () {
       
    }

    public void startPipes() {
        foreach(Pipe child in GetComponentsInChildren<Pipe>()) {
            child.startPipe();
        }
    }

    public void createPipes(float curX) {
        var y = Random.Range (-0.45f, 1.5f);
        Vector3 pos = new Vector3(curX + GameManager.pipeInterval, y, 0);

        //Instantiate a new pipe
        GameObject newPipe = (GameObject) Instantiate (pipePrefab, pos, Quaternion.identity);
        newPipe.transform.parent = gameObject.transform;
        startPipes ();
    }

    public void stopPipes() {
        foreach(Pipe child in GetComponentsInChildren<Pipe>()){
            child.stopPipe();
        }
    }
}


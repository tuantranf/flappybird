﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static float jump = 3.3f;
	public static float jumGravity = 1.1f;
    public static float jumAngle = Mathf.Lerp (0, 25, 1);
    public static float speed = 0.02f;
    public static float pipeInterval = 1.55f;
    public static float deletePosX = -2f;

    private static int gameStatus = -1; //-1:title 0:ready 1:game 2:gameover
    public static int GameStatus {
        get { return gameStatus; }
        set { gameStatus = value; }
    }

    public int score;

    public GameObject gameOverPrefab;
    public GameObject gameBoardPrefab;

    GameObject thePipes;
    GameObject gameTitle;
    GameObject gameScore;
    GameObject gameResult;
    GameObject theBackground;
    GameObject theBird;

    AudioSource sound;

    void Awake() {
        QualitySettings.vSyncCount = 0; // VSyncをOFFにする
        Application.targetFrameRate = 60; // ターゲットフレームレートを60に設定
    }

    // Use this for initialization
    void Start () {
        thePipes = GameObject.Find ("Pipes");
        gameTitle = GameObject.Find ("GameTitle");
        gameScore = GameObject.Find ("GameScore");
        gameResult = GameObject.Find ("GameResult");
        theBackground = GameObject.Find ("Background");
        theBird = GameObject.Find ("Bird");

        sound = GameObject.Find ("MainCamera").GetComponent<AudioSource> ();

        score = 0;
        GameStatus = 0;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetMouseButton(0)) {
            Debug.Log("GameStatus: " + GameStatus);
            if (GameStatus == 0) {
                gameStart ();
            }
        }
    }

    public void gameStart() {
        if (GameStatus == 1) {
            return;
        }

        gameTitle.GetComponent<Canvas>().enabled = false;
        theBird.gameObject.AddComponent<Rigidbody2D> ();
        theBird.gameObject.rigidbody2D.gravityScale = jumGravity;
        theBird.gameObject.rigidbody2D.fixedAngle = true;
		thePipes.GetComponent<PipeManager> ().createPipes (2.5f);
		thePipes.GetComponent<PipeManager> ().createPipes (2.5f + pipeInterval);
        
        GameStatus = 1;
    }
    
    public void gameOver(int score) {
        if (GameStatus == 2) {
            return;
        }
        GameStatus = 2;
        
        thePipes.GetComponent<PipeManager>().stopPipes();
        gameScore.GetComponent<Canvas>().enabled = false;
        theBackground.GetComponent<Background>().stopGrounds();

		StartCoroutine ("ShowResult", score);
    }

    private IEnumerator ShowResult(int score) {

        yield return new WaitForSeconds (0.8f);

        sound.Play ();
        gameResult.GetComponent<Canvas>().enabled = true;

        Instantiate (gameOverPrefab, gameOverPrefab.transform.position, gameOverPrefab.transform.rotation);

        yield return new WaitForSeconds (0.6f);

        sound.Play ();  
        Instantiate (gameBoardPrefab, gameBoardPrefab.transform.position, gameBoardPrefab.transform.rotation);

        yield return new WaitForSeconds (0.6f);
        GameObject.Find ("GameResult/ResultBoard").GetComponent<Canvas>().enabled = true;
		GameObject.Find ("GameResult/ResultBoard").GetComponent<Canvas> ().overrideSorting = true;

        gameResult.GetComponent<GameResultManager> ().updateScores (score);

        yield return new WaitForSeconds (0.2f);

        GameObject.Find ("GameResult/Start").GetComponent<Canvas>().enabled = true;
    }
}
